#ifndef _MXGEN_LEVEL0_LINE_ENDING_INCLUDE_
#define _MXGEN_LEVEL0_LINE_ENDING_INCLUDE_

#include "mxgen_L0_plat.h"

/* FIXME This still doesn't handle super obscure cases. */

/* Ancient teletype line endings with Microsoft. */
#if defined(MXGEN_LEVEL0_PLATFORM_FAMILY_MICROSOFT)
#define _MXSUB_ENDOFLINE     "\r\n"
#define _MXSUB_ENDOFLINE_LEN 2
#define _MXSUB_ENDOFLINE_TWOBYTE
/* Otherwise, we assume UNIX line endings. */
#else
#define _MXSUB_ENDOFLINE     "\n"
#define _MXSUB_ENDOFLINE_LEN 1
#define _MXSUB_ENDOFLINE_ONEBYTE
#endif

#endif