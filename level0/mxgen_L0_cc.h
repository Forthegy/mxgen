#ifndef _MXGEN_LEVEL0_CC_INCLUDE_
#define _MXGEN_LEVEL0_CC_INCLUDE_

#include "mxgen_L0_plat.h"

/* We have an "ugly" C compiler with no support for common attributes. */
#if defined(MXGEN_LEVEL0_CUGLY)
# define MXGEN_LEVEL0_CC_HAVE_UGLY
/* We have Clang, which supports common attributes for all versions in common
 * use. */
#elif defined(__clang__)
# define MXGEN_LEVEL0_CC_CLANG
# define MXGEN_LEVEL0_CC_HAVE_COMMON_ATTRIBUTES
/* We have GNU C, which supports common attributes in major versions exceeding
 * version 4. */
#elif defined(__GNUC__)
# define MXGEN_LEVEL0_CC_GNUC
/* Okay, with version >4 we have common attributes. */
# if __GNUC__ > 4
#   define MXGEN_LEVEL0_CC_GNU4
#   define MXGEN_LEVEL0_CC_HAVE_COMMON_ATTRIBUTES
# endif
#endif

/* Non-compliant Microsoft subset of C89. */
#if defined(MXGEN_LEVEL0_CSTD) || defined(MXGEN_LEVEL0_CUGLY)
#define MXGEN_LEVEL0_CC_HAVE_NONCOMPLIANTC89
#endif

/* Full C89. */
#if defined(MXGEN_LEVEL0_CSTD)
#define MXGEN_LEVEL0_CC_HAVE_STDC89
#endif

/* Common function attributes.
 *   PURE  On supporting compilers, as long as the state of the program does not
 *         change between calls, this procedure will yield the same return value
 *         for the same arguments.
 *   CONST On supporting compilers, no matter the state of the program, this
 *         procedure will yield the same return value for the same arguments. 
 *   WEAK  On supporting compilers, this procedure will be identified by a weak
 *         symbol, meaning that it is not propagated to linked objects who
 *         contain symbols of the same name. If there is no compiler support for
 *         the notion of a weak symbol, the desired behavior can be obtained by
 *         using the inline directive (but usually in a manner that leads to
 *         larger code sizes). */
#if defined(MXGEN_LEVEL0_CC_HAVE_UGLY)
#define MXGEN_LEVEL0_PURE
#define MXGEN_LEVEL0_CONST 
#define MXGEN_LEVEL0_WEAK  __inline
#elif defined(MXGEN_LEVEL0_CC_HAVE_COMMON_ATTRIBUTES)
#define MXGEN_LEVEL0_PURE  __attribute__ ((pure))
#define MXGEN_LEVEL0_CONST __attribute__ ((const))
#define MXGEN_LEVEL0_WEAK  __attribute__ ((weak))
#elif defined(MXGEN_LEVEL0_CC_HAVE_C89)
#define MXGEN_LEVEL0_PURE
#define MXGEN_LEVEL0_CONST 
#define MXGEN_LEVEL0_WEAK  inline
#else
# error "mxgen level 0 does not recognize the C environment"
#endif

#endif /* _MXGEN_LEVEL0_CC_INCLUDE_ */
