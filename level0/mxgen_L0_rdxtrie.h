#ifndef _MXGEN_LEVEL0_RDXTRIE_INCLUDE_
#define _MXGEN_LEVEL0_RDXTRIE_INCLUDE_

#include "mxgen_L0_base.h"

/* TODO Fix documentation where constant types are declared mutable by the
 *      documentation. */

/**
 * PushSection:
 *   MxGen Level 0 Radix Trie
 * Description:
 *   Radix Trie implementation for MxGen property storage. */

#define MXGEN_RADIXTRIE_MAX_KEY_LENGTH 256UL

/** Forward declaration of the radix trie node structure. */
struct mxgen_radixtrie_node;

/**
 * The Radix Trie node array storage is the internal allocation pool mechanism
 * for reducing fragmentation of radix trie nodes in the linked structure. This
 * pool does not have any mechanism for freeing the memory except all at once.
 * When a node is no longer in use by the radix trie structure, it will be kept
 * in a "free stack" in the high level pool structure using the builtin
 * "pnextfree" pointer of the radix trie node structure. */
struct mxgen_radixtrie_node_array
{
  /** Next node array. */
  struct mxgen_radixtrie_node_array *pnext;
  /** Pointer to the nodes. */
  struct mxgen_radixtrie_node *pnodes;
  /** Pointer to the next unallocated node. */
  struct mxgen_radixtrie_node *punallocnode;
  /** Number of nodes. */
  long n;
  /** Exhaustion flag. */
  int bexhausted;
};

/** Radix trie node structure. */
struct mxgen_radixtrie_node
{
  /** Reference counter. */
  unsigned long refcount;

  /** Point to the next free node. NULL if not presently in the free stack. */
  struct mxgen_radixtrie_node const *pnextfree;

  /**
   * The symbol for the child link. If child_sym is zero, this is a
   * terminal node. */
  int child_sym;

  /** Pointer to the next sibling. */
  struct mxgen_radixtrie_node *psib;
  /** Pointer to the child associated with the symbol. */
  struct mxgen_radixtrie_node *pchild;

  /**
   * Value, if any associated with this radix trie node. This is valid for both
   * terminal and non-terminal nodes. If null, then this node does not represent
   * a key in the trie. If non-null, then it represents a key in the trie whose
   * associated value is the address. */
  void *pvalue;
};

/** 
 * This is the top level allocation pool structure for radix trie nodes. It
 * keeps a list of nodes that are allocated, but not presently in use, called
 * the free stack. Because the space is reserved in the node structure to
 * account for this singly linked list, and that singly linked list is treated
 * as a stack of available nodes to be preferred over unallocated nodes from
 * the internal pool, the tendency of the node allocation mechanism will be to
 * present nodes whose addresses are extremely close to one another, the aim of
 * which is to improve cache performance. In the case that the free stack is
 * empty, the underlying internal allocation pool will allocate a new node, and
 * grow if neccessary to accomodate adequate space for it. */
struct mxgen_radixtrie_pool
{
  /** First array of the node array list. */
  struct mxgen_radixtrie_node_array mnodearrays;
  /** The current tail of the node array list is cached here. */
  struct mxgen_radixtrie_node_array *ptailnodearray;

  /** Sentinel node of the free stack. */
  struct mxgen_radixtrie_node mfreestack_sentinel;
};

/**
 * Procedure(weaksym):
 *   mxgen_radixtrie_node_array_init
 * Description:
 *   Initialize a radix trie node array. This must be called to initialize the
 *   structure and allocate the array.
 * Arguments:
 *   1 struct mxgen_radixtrie_node_array m*i.
 *     Pointer to the radix trie node array structure.
 *   2 long i.
       Desired capacity. Note that this will be rounded up to the next power of
 *     two.
 * Return(int):
 *    0 : Success
 *   -N : Bad parameter N
 *   +N : Error
 */
MXGEN_LEVEL0_WEAK int
  mxgen_radixtrie_node_array_init(
    struct mxgen_radixtrie_node_array *const,
    long);

/**
 * Procedure(weaksym):
 *   mxgen_radixtrie_node_array_uninit
 * Description:
 *   Uninitialize a radix trie node array. This must be called to deallocate the
 *   node array and any chained subarrays.
 * Arguments:
 *   1 struct mxgen_radixtrie_node_array m*i.
 *     Pointer to the radix trie node array structure.
 * Return(int):
 *    0 : Success
 *   -N : Bad parameter N
 *   +N : Error
 */
MXGEN_LEVEL0_WEAK int
  mxgen_radixtrie_node_array_uninit(
    struct mxgen_radixtrie_node_array *const);

/**
 * Procedure(weaksym):
 *   mxgen_radixtrie_node_array_grow
 * Description:
 *   Initialize a new subarray. This must be called if more nodes are needed
 *   than are available in the existing array and subarrays. Each subsequent
 *   subarray will contain twice the number of nodes as were contained in the
 *   previous array. Thus, the number of arrays will grow logarithmically with
 *   respect to the number of required simultaneously allocated nodes.
 * Arguments:
 *   1 struct mxgen_radixtrie_node_array m*i.
 *     Pointer to the radix trie node array structure.
 *   2 struct mxgen_radixtrie_node_array m*m*i.
 *     Pointer address for the last subarray.
 * Return(int):
 *    0 : Success
 *   -N : Bad parameter N
 *   +N : Error
 */
MXGEN_LEVEL0_WEAK int
  mxgen_radixtrie_node_array_grow(
    struct mxgen_radixtrie_node_array *const,
    struct mxgen_radixtrie_node_array **const);

/**
 * Procedure(weaksym):
 *   mxgen_radixtrie_node_array_alloc
 * Description:
 *   Permenantly allocate a node from the node array structure. If the structure
 *   is exhausted, call mxgen_radixtrie_node_array_grow to initialize a new
 *   subarray, and then proceed with the allocation.
 * Arguments:
 *   1 struct mxgen_radixtrie_node_array m*i.
 *     Pointer address for the radix trie node array structure. The pointer may
 *     be mutated if mxgen_radixtrie_node_array_grow is called.
 *   2 struct mxgen_radixtrie_node m*m*i.
 *     Pointer address for the allocated node pointer to be written.
 * Return(int):
 *    0 : Success
 *   -N : Bad parameter N
 *   +N : Error
 */
MXGEN_LEVEL0_WEAK int
  mxgen_radixtrie_node_array_alloc(
    struct mxgen_radixtrie_node_array **const,
    struct mxgen_radixtrie_node const **const);

/**
 * Procedure(weaksym):
 *   mxgen_radixtrie_pool_init
 * Description:
 *   Initialize a radix trie pool. This must be called before the pool is
 *   usable.
 * Arguments:
 *   1 struct mxgen_radixtrie_pool m*i.
 *     Pointer to the radix trie pool structure.
 *   2 long i.
 *     Desired initial capacity. Note that this will be rounded up to the next
 *     power of two.
 * Return(int):
 *    0 : Success
 *   -N : Bad parameter N
 *   +N : Error
 */
MXGEN_LEVEL0_WEAK int
  mxgen_radixtrie_pool_init(
    struct mxgen_radixtrie_pool *const,
    long);

/**
 * Procedure(weaksym):
 *   mxgen_radixtrie_pool_uninit
 * Description:
 *   Uninitialize a radix trie pool. This must be called to deallocate all of
 *   the memory in use by the pool.
 * Arguments:
 *   1 struct mxgen_radixtrie_pool m*i.
 *     Pointer to the radix trie pool structure.
 * Return(int):
 *    0 : Success
 *   -N : Bad parameter N
 *   +N : Error
 */
MXGEN_LEVEL0_WEAK int
  mxgen_radixtrie_pool_uninit(
    struct mxgen_radixtrie_pool *const);

/**
 * Procedure(weaksym):
 *   mxgen_radixtrie_pool_take_node
 * Description:
 *   Get a free node either by recycling a node from the free stack or by
 *   allocating a new node from the node array list.
 * Arguments:
 *   1 struct mxgen_radixtrie_pool m*i.
 *     Pointer to the radix trie pool structure.
 *   2 struct mxgen_radixtrie_node m*m*i.
 *     Pointer address for the pointer to the fetched node to be written.
 * Return(int):
 *    0 : Success
 *   -N : Bad parameter N
 *   +N : Error
 */
MXGEN_LEVEL0_WEAK int
  mxgen_radixtrie_pool_take_node(
    struct mxgen_radixtrie_pool *const,
    struct mxgen_radixtrie_node const **const);

/**
 * Procedure(weaksym):
 *   mxgen_radixtrie_pool_drop_node
 * Description:
 *   Return a node in to circulation by clearing it and pushing it on to the
 *   free stack.
 * Arguments:
 *   1 struct mxgen_radixtrie_pool m*i.
 *     Pointer to the radix trie pool structure.
 *   2 struct mxgen_radixtrie_node m*i.
 *     Pointer to the node to be returned to circulation.
 * Return(int):
 *    0 : Success
 *   -N : Bad parameter N
 *   +N : Error
 */
MXGEN_LEVEL0_WEAK int
  mxgen_radixtrie_pool_drop_node(
    struct mxgen_radixtrie_pool *const,
    struct mxgen_radixtrie_node const *const);

/* TODO document me */
MXGEN_LEVEL0_WEAK unsigned long
  mxgen_radixtrie_node_ref(
    struct mxgen_radixtrie_pool *const,
    struct mxgen_radixtrie_node const **const,
    int *const status);

/* TODO document me */
MXGEN_LEVEL0_WEAK unsigned long
  mxgen_radixtrie_node_unref(
    struct mxgen_radixtrie_pool *const,
    struct mxgen_radixtrie_node const **const,
    int *const status);

/**
 * Procedure(weaksym):
 *   mxgen_radixtrie_put
 * Description:
 *   Given a pointer to a radix trie node, a radix trie node pool, a UTF-8
 *   string key, and a value pointer, return a radix trie node which points to a
 *   new radix trie containing the given key-value pair. If the value pointer is
 *   NULL, then the key will be erased, if it exists.
 * Arguments:
 *   1 struct mxgen_radixtrie_pool m*i.
 *     Pointer to the radix trie pool structure.
 *   2 char i*i.
 *     Key string.
 *   3 void m*i.
 *     Value address.
 *   4 struct mxgen_radixtrie_node i*i.
 *     Pointer to the root node of the radix trie.
 *   5 int m*i.
 *     Pointer to the operation status.
 *       0 : Success
 *      -N : Bad parameter N
 *      +N : Error
 * Return(struct mxgen_radixtrie_node const *):
 *   The root node of the new trie after the put operation.
 */
MXGEN_LEVEL0_WEAK struct mxgen_radixtrie_node const *
  mxgen_radixtrie_put(
    struct mxgen_radixtrie_pool *const,
    char const *const,
    void *,
    struct mxgen_radixtrie_node const *const,
    int *const);

/**
 * Procedure(weaksym):
 *   mxgen_radixtrie_erase
 * Description:
 *   Given a pointer to a radix trie node, a radix trie node pool, and a UTF-8
 *   string key, return a radix trie node which points to a new radix trie with
 *   the given key erased. This operation is merely an alias of
 *   mxgen_radixtrie_put with a value address of NULL.
 * Arguments:
 *   1 struct mxgen_radixtrie_pool m*i.
 *     Pointer to the radix trie pool structure.
 *   2 char i*i.
 *     Key string.
 *   3 struct mxgen_radixtrie_node i*i.
 *     Pointer to the root node of the radix trie.
 *   4 int m*i.
 *     Pointer to the operation status.
 *       0 : Success
 *      -N : Bad parameter N
 *      +N : Error
 * Return(struct mxgen_radixtrie_node const *):
 *   The root node of the new trie after the erase operation.
 */
MXGEN_LEVEL0_WEAK struct mxgen_radixtrie_node const *
  mxgen_radixtrie_erase(
    struct mxgen_radixtrie_pool *const,
    char const *const,
    struct mxgen_radixtrie_node const *const,
    int *const);

/**
 * PopSection */

/*******************************************/
/* Implementation of Node Array Procedures */
/*******************************************/

int mxgen_radixtrie_node_array_init(
    struct mxgen_radixtrie_node_array *const parray,
    long cap)
{
  void *p;

  if (NULL == parray)
    return -1; /* Bad first argument */
  if (cap < 0)
    return -2; /* Bad second argument */

  /* Get next power of two */
  --cap;
  cap |= cap >> 1;
  cap |= cap >> 2;
  cap |= cap >> 4;
  cap |= cap >> 8;
  cap |= cap >> 16;
  ++cap;

  /* Heap allocation of address list */
  p = (void *)malloc(sizeof(struct mxgen_radixtrie_node)*cap);
  if (NULL == p)
    return 1; /* Heap allocation error */

  (void)mxgen_memclear(parray, sizeof(struct mxgen_radixtrie_node_array));

  parray->pnext = NULL;
  parray->n = cap;
  parray->pnodes = (struct mxgen_radixtrie_node *)p;
  parray->punallocnode = parray->pnodes;
  parray->bexhausted = 0;

  return 0;
}

int mxgen_radixtrie_node_array_uninit(
    struct mxgen_radixtrie_node_array *const parray)
{
  struct mxgen_radixtrie_node_array *pcurr;
  struct mxgen_radixtrie_node_array *pnext;

  if (NULL == parray)
    return -1; /* Bad first argument. */

  pcurr = parray;
  while (NULL != pcurr)
  {
    pnext = pcurr->pnext; 
    if (NULL != pcurr->pnodes)
      (void)free((void *)pcurr->pnodes);

    (void)mxgen_memclear(pcurr, sizeof(struct mxgen_radixtrie_node_array));

    if (pcurr != parray)
      /* The child nodes are dynamically allocated, free them too. */
      (void)free((void *)pcurr);

    pcurr = pnext;
  }

  return 0;
}

int mxgen_radixtrie_node_array_grow(
    struct mxgen_radixtrie_node_array *const parray,
    struct mxgen_radixtrie_node_array **const ppnewtail)
{
  int result;
  struct mxgen_radixtrie_node_array *pcurr;
  struct mxgen_radixtrie_node_array *pnext;

  if (NULL == parray)
    return -1; /* Bad first argument. */

  if (NULL == ppnewtail)
    return -2; /* Bad second argument. */

  pcurr = parray;
  while (1)
  {
    if (pcurr->pnext == NULL)
      /* Reached tail. */
      break;

    pcurr = pcurr->pnext;
  }

  pnext = (struct mxgen_radixtrie_node_array *)
    malloc(sizeof(struct mxgen_radixtrie_node_array));

  if (NULL == pnext)
    return 1; /* Bad allocation. */

  result = mxgen_radixtrie_node_array_init(pnext, pcurr->n * 2L);
  if (0 == result)
    pcurr->pnext = *ppnewtail = pnext;

  return result;
}

int mxgen_radixtrie_node_array_alloc(
    struct mxgen_radixtrie_node_array **const pparray,
    struct mxgen_radixtrie_node const **const ppnode)
{
  int result;
  struct mxgen_radixtrie_node_array *ptail;

  if (NULL == pparray)
    return -1; /* Bad first argument (node array pointer address must be
                * non-zero). */

  if (NULL == *pparray)
    return -1; /* Bad first argument (node array pointer must be non-zero). */

  if (NULL == ppnode)
    return -2; /* Bad second argument (node pointer address must be non-zero).
                * */

  /* Get the tail. */
  ptail = *pparray;
  while (NULL != ptail->pnext)
    ptail = ptail->pnext;

  /* Grow if and only if the tail is exhausted. */
  result = ptail->bexhausted
         ? mxgen_radixtrie_node_array_grow(ptail, &ptail)
         : 0;

  if (0 == result)
  {
    /* The grow, if any, succeeded. Grab the next node. */
    *ppnode = ptail->punallocnode++;

    /* If the unallocated node pointer has advanced n positions from the array
     * pointer, then this subarray is exhausted. */
    if (ptail->punallocnode - ptail->pnodes >= ptail->n)
      ptail->bexhausted = 1;

    /* Write the current tail pointer at the pointer address. */
    *pparray = ptail;
  }

  return result;
}

/*************************************/
/* Implementation of Pool Procedures */
/*************************************/

int mxgen_radixtrie_pool_init(
    struct mxgen_radixtrie_pool *const ppool,
    long cap)
{
  int result;

  if (NULL == ppool)
    return -1; /* Bad first argument. */

  if (cap <= 0)
    return -2; /* Bad second argument. */

  mxgen_memclear(ppool, sizeof(struct mxgen_radixtrie_pool));

  result = mxgen_radixtrie_node_array_init(&ppool->mnodearrays, cap);

  if (0 == result)
  {
    ppool->ptailnodearray = &ppool->mnodearrays;
    ppool->mfreestack_sentinel.pnextfree = NULL;
  }

  return result;
}

int mxgen_radixtrie_pool_uninit(
    struct mxgen_radixtrie_pool *const ppool)
{
  if (NULL == ppool)
    return -1; /* Bad first argument. */

  return mxgen_radixtrie_node_array_uninit(&ppool->mnodearrays);
}

int mxgen_radixtrie_pool_take_node(
    struct mxgen_radixtrie_pool *const ppool,
    struct mxgen_radixtrie_node const **const ppnode)
{
  int result;
  struct mxgen_radixtrie_node const *pnode;

  if (NULL == ppool)
    return -1; /* Bad first argument. */

  if (NULL == ppnode)
    return -2; /* Bad second argument. */

  if (NULL != ppool->mfreestack_sentinel.pnextfree)
  {
    /* The free stack is not empty, pop a node off the top. */
    pnode = ppool->mfreestack_sentinel.pnextfree;
    ppool->mfreestack_sentinel.pnextfree = pnode->pnextfree;

    /* Clear the memory. */
    mxgen_memclear(pnode, sizeof(struct mxgen_radixtrie_node));

    /* Success, write the node pointer out. */
    result = 0;
    *ppnode = pnode;
  }
  else
  {
    /* Free stack empty, resort to allocating new nodes. */
    result = mxgen_radixtrie_node_array_alloc(&ppool->ptailnodearray, &pnode);
    if (0 == result) *ppnode = pnode;
  }

  return result;
}

int mxgen_radixtrie_pool_drop_node(
    struct mxgen_radixtrie_pool *const ppool,
    struct mxgen_radixtrie_node const *const pnode)
{
  struct mxgen_radixtrie_node *const pmnode =
    (struct mxgen_radixtrie_node *)pnode;

  if (NULL == ppool)
    return -1; /* Bad first argument. */

  if (NULL == pnode)
    return -2; /* Bad second argument. */

  if (NULL != pnode->pnextfree)
    return -2; /* Bad second argument (getting dropped twice!) */

  /* Ensure the memory is cleared. */
  mxgen_memclear(pnode, sizeof(struct mxgen_radixtrie_node));

  /* Push the node on to the free stack. */
  pmnode->pnextfree = ppool->mfreestack_sentinel.pnextfree;
  ppool->mfreestack_sentinel.pnextfree = pnode;

  return 0;
}

/******************************************/
/* Implementation of Trie Node Procedures */
/******************************************/

unsigned long mxgen_radixtrie_node_ref(
    struct mxgen_radixtrie_pool *const ppool,
    struct mxgen_radixtrie_node const **const ppnode,
    int *const pstatus)
{
  int _sink, *ps; /* status return */
  struct mxgen_radixtrie_node *pnewnode;

  ps = NULL == pstatus
     ? &_sink
     : pstatus;

  if (NULL == ppool)
  {
    *ps = -1; /* bad first argument */
    return 0UL;
  }

  if (NULL == ppnode)
  {
    *ps = -2; /* bad second argument */
    return 0UL;
  }

  if (NULL == *ppnode)
  {
    if (0 == (*ps = mxgen_radixtrie_pool_take_node(ppool, &pnewnode)))
    {
      pnewnode->refcount = 1UL;
      *ppnode = (struct mxgen_radixtrie_node const *)pnewnode;
      return 1UL;
    }
    else
      return 0UL;
  }
  else
  {
    pnewnode = (struct mxgen_radixtrie_node *)*ppnode;
    return ++pnewnode->refcount; 
  }
}

unsigned long mxgen_radixtrie_node_unref(
    struct mxgen_radixtrie_pool *const ppool,
    struct mxgen_radixtrie_node const **const ppnode,
    int *const pstatus)
{
  struct mxgen_radixtrie_node *pnode;
  struct mxgen_radixtrie_node const *pchild;
  struct mxgen_radixtrie_node const *psib;
  int _sink, *ps;

  ps = NULL != pstatus
     ? pstatus
     : &_sink;

  *ps = 0;

  if (NULL == ppool)
  {
    *ps = -1; /* bad first argument */
    return 0UL;
  }

  if (NULL == ppnode)
  {
    *ps = -2; /* bad second argument */
    return 0UL;
  }

  pnode = (struct mxgen_radixtrie_node *)*ppnode;

  if (0 == --pnode->refcount)
  {
    /* we've dropped the last reference */

    /* unreference sibling and child, they'll no longer be referenced by this
     * node. */
    if (NULL != (pchild = pnode->pchild))
      (void)mxgen_radixtrie_node_unref(ppool, &pchild, ps);
    if (NULL != (psib = pnode->psib))
      (void)mxgen_radixtrie_node_unref(ppool, &psib, ps);

    /* drop the node */
    *ps = mxgen_radixtrie_pool_drop_node(ppool, pnode);
    if (0 == *ps) *ppnode = NULL;
    return 0UL;
  }
  else
    return pnode->refcount;
}

struct _mxgen_radixtrie_put_stack_entry
{
  /* Node associated with this stack entry or NULL if no node exists at this
   * level. If pnode is NULL, then sym must be non-zero. */
  struct mxgen_radixtrie_node const *pnode;
  int sym; /* 0 if prior node is a sibling, or a symbol number if prior node
            * is a parent whose child_sym in non-zero. */
};

/* Internal: a new trie consisting of one finger. */
MXGEN_LEVEL0_WEAK struct mxgen_radixtrie_node *_mxgen_radixtrie_new(
    struct mxgen_radixtrie_pool *const ppool,
    char const *const skey,
    void *pvalue,
    int *const pstatus,
    unsigned long keylength)
{
  char const *pkey;
  unsigned long refcount;
  unsigned long i;
  struct mxgen_radixtrie_node basenode;
  struct mxgen_radixtrie_node *pnode;

  /* Empty subtrie */
  if (NULL == pvalue)
    return NULL;

  /* Initialize base sentinel and key pointer. */
  basenode.pchild = NULL;
  pnode = &basenode;
  pkey = skey;

  /* Traverse the key. */
  for (i = 0; i < keylength; ++i)
  {
    refcount = mxgen_radixtrie_node_ref(ppool, &pnode->pchild, pstatus);
    if (refcount != 1 || *pstatus != 0)
      return basenode.pchild;
    pnode = pnode->pchild;
    pnode->child_sym = *pkey++;
  }
  refcount = mxgen_radixtrie_node_ref(ppool, &pnode->pchild, pstatus);
  if (refcount != 1 || *pstatus != 0)
    return basenode.pchild;
  pnode->pchild->pvalue = pvalue;
  return basenode.pchild;
}

struct mxgen_radixtrie_node const *mxgen_radixtrie_put(
    struct mxgen_radixtrie_pool *const ppool,
    char const *const skey,
    void *pvalue,
    struct mxgen_radixtrie_node const *const pnode,
    int *const pstatus)
{
  struct _mxgen_radixtrie_put_stack_entry *pstack;

  char const *pkey;  
  int _sink, *ps; /* Status */
  unsigned long refcount;
  unsigned long keylength;
  unsigned long i, n;
  int curr_sym, b;
  struct mxgen_radixtrie_node const *plocalnode;
  struct mxgen_radixtrie_node *pmutablenode;

  ps = NULL != pstatus
     ? pstatus
     : &_sink;

  *ps = 0;

  if (NULL == ppool)
  {
    *ps = -1; /* Bad first argument. */
    return NULL;
  }

  if (NULL == skey)
  {
    *ps = -2; /* Bad second argument. */
    return NULL;
  }

  /* Get the length of the key relatively safely. */
  keylength = mxgen_strnlen(skey, MXGEN_RADIXTRIE_MAX_KEY_LENGTH);

  /* TODO Check that pnode actually points to somewhere managed by the pool. */
  /* NOTE Perhaps that can be handled by ref/unref? */

  plocalnode = pnode;

  if (NULL == plocalnode)
    /* There is no trie; build a lone new finger and return it. */
    return _mxgen_radixtrie_new(ppool, skey, pvalue, ps, keylength);

  /* TODO: handle the degenerate case of the empty key here. */

  /* It's an old node. We need to construct a new trie. First we have to
    * find out the path through the existing structure. On the first pass,
    * we will simply count the number of nodes to traverse and perform a
    * stack frame allocation to keep them in a proper stack. */

  i = 0;       /* Use i as the key character counter. */
  n = 1;       /* Use n as the stack depth counter. */
  pkey = skey; /* We need a mutable pointer to the key to advance. */
  while (b && i++ < keylength)
  {
    /* Get the current symbol. */
    curr_sym = (int)*pkey++;

    /* Look through the sibling list for the first younger sibling whose symbol
     * is less than or equal to the current symbol. */
    while (b && plocalnode->child_sym < curr_sym)
    {
      ++n; /* The current symbol is greater than or equal to the symbol of the
            * present sibling, so one more stack element will be needed to keep
            * track of it. */
      /* Advance to the next sibling. */
      plocalnode = plocalnode->psib;
      /* If the next sibling is NULL, then there is no next sibling. Therefore,
       * the current symbol is greater than or equal to the symbols of all of
       * the siblings in the sibling list. Furthermore, because of the sentinal
       * condition, the last sibling must have had a symbol that was less than
       * the current symbol. So this next symbol being greater than or equal
       * means the sentinel will abort the loop. In that case, if there was an
       * equal node, then that means the current value of plocalnode will point
       * to a node whose child_sym value is equal to the current symbol. */
      if (NULL == plocalnode)
        b = 0;
    }
    /* When we reach this point, if plocalnode is NULL, then there can't have
     * been any equal symbol in the sibling list because all the symbols were
     * less than the current symbol. In that case, we will insert the new
     * subtrie at the end of a new finger traversing this sibling list and we
     * are done counting the needed stack space. */
    if (b && plocalnode->child_sym == curr_sym)
    {
      /* If plocalnode points to a node whose symbol is equal to the current
       * symbol, then we have found an existing subtrie with the same prefix as
       * that of the already traversed part of the key. In that case, we move
       * deeper in to the trie and keep counting. But if that subtrie is NULL,
       * then we count only one more needed stack slot, and stop. */
      ++n;
      plocalnode = plocalnode->pchild;
      if (NULL == plocalnode)
        b = 0;
    }
  }

  if (i < keylength && NULL == pvalue)
    goto SKIP; /* Don't do anything if we've been asked to delete a key but
                * didn't actually find it. */

  /* Now that we've scoped out how much space we need to keep track of the
   * replacement finger, we grow the stack to accomodate and run up the stack
   * filling it out as we go. We follow the same logic as before while filling
   * in the stack, but rather than incrementing a counter, this time we write
   * the nodes we traverse to the stack. */

  /* Grow the stack enough to keep track of the original nodes whose contents
   * are to be partially copied to the replacement finger. */
  pstack = (struct _mxgen_radixtrie_put_stack_entry *)mxgen_alloca(
    (size_t)(n+1)*sizeof(struct _mxgen_radixtrie_put_stack_entry));

  /* reset trace */
  b = 1; /* sentinel value */
  i = 0;
  pkey = skey;

  /* write sentinel values at the bottom of the stack. */
  pstack->sym = 0;
  pstack->pnode = NULL;
  ++pstack;

  /* build up the stack. */
  while (b && i++ < keylength)
  {
    curr_sym = (int)*pkey++;
    while (b && plocalnode->child_sym < curr_sym)
    {
      pstack->sym = 0; /* zero symbol demarks a sibling traversal */
      pstack->pnode = plocalnode;
      ++pstack;
      plocalnode = plocalnode->psib;
      if (NULL == plocalnode)
        b = 0;
    }
    if (b && plocalnode->child_sym == curr_sym)
    {
      pstack->sym = curr_sym;
      pstack->pnode = plocalnode;
      ++pstack;
      plocalnode = plocalnode->pchild;
      if (NULL == plocalnode)
        b = 0;
    }
  }

  /* If curr_sym is at the top of the stack, then we don't need to include it
   * in the new subtrie finger. Otherwise, we do. Reuse b to keep track of this.
   * */
  if (b = (0 == pstack[-1].sym))
  {
    /* This will be stitched in to a sibling list; we need the current symbol
     * as part of the new subtrie. */
    --pkey;
    --i;
  }

  /* Build the new subtrie finger. */
  plocalnode = _mxgen_radixtrie_new(ppool, pkey, pvalue, ps, keylength - i);

  /* Now we unwind the stack, stitching the new subtrie finger in by making
   * clone nodes along the finger leading to the point where it should be 
   * attached. In this way, we build a completely new trie with only N new nodes
   * where N is the length of the key. So the radix trie is immutable and any
   * put operation will return a new root node despite having only linear
   * space and time complexities w.r.t. the key length, unless the put operation
   * is such that it will not result in any change. */

  /* NOTE we reuse i as a reference count. */

  if (!b) goto STITCH_CHILDREN;

STITCH_SIBLINGS:
  /* Sibling stitching loop. */
  while (NULL != pstack[-1].pnode && 0 == pstack[-1].sym)
  {
    pmutablenode = NULL;
    i = mxgen_radixtrie_node_ref(ppool, &pmutablenode, ps);
    if (1UL != i || 0 != *ps)
    {
      (void)mxgen_radixtrie_node_unref(ppool, &plocalnode, &_sink);
      return pnode; /* Catastrophic failure to allocate. */
    }

    mxgen_memcpy(pmutablenode, pstack[-1].pnode,
      sizeof(struct mxgen_radixtrie_node));

    pmutablenode->refcount = i;
    pmutablenode->psib = (struct mxgen_radixtrie_node *)plocalnode;

    if (NULL != pmutablenode->pchild)
    {
      i = mxgen_radixtrie_node_ref(ppool, &pmutablenode->pchild, ps);
      if (1UL == i || 0 != *ps)
      {
        (void)mxgen_radixtrie_node_unref(ppool, &pmutablenode, &_sink);
        return pnode; /* Catastrophic failure to allocate. */
      }
    }

    plocalnode = pmutablenode;
    --pstack;
  }

STITCH_CHILDREN:
  /* Child stitching loop. */
  while (NULL != pstack[-1].pnode && 0 != pstack[-1].sym)
  {
    pmutablenode = NULL;
    i = mxgen_radixtrie_node_ref(ppool, &pmutablenode, ps);
    if (1UL != i || 0 != *ps)
    {
      (void)mxgen_radixtrie_node_unref(ppool, &plocalnode, &_sink);
      return pnode; /* Catastrophic failure to allocate. */
    }

    mxgen_memcpy(pmutablenode, pstack[-1].pnode,
      sizeof(struct mxgen_radixtrie_node));

    pmutablenode->refcount = i;
    pmutablenode->pchild = (struct mxgen_radixtrie_node *)plocalnode;

    if (NULL != pmutablenode->psib)
    {
      i = mxgen_radixtrie_node_ref(ppool, &pmutablenode->psib, ps);
      if (1UL == i || 0 != *ps)
      {
        (void)mxgen_radixtrie_node_unref(ppool, &pmutablenode, &_sink);
        return pnode; /* Catastrophic failure to allocate. */
      }
    }

    plocalnode = pmutablenode;
    --pstack;
  }

  if (NULL != pstack[-1].pnode) goto STITCH_SIBLINGS;

SKIP:
  return plocalnode;
}

struct mxgen_radixtrie_node const *mxgen_radixtrie_erase(
    struct mxgen_radixtrie_pool *const ppool,
    char const *const skey,
    struct mxgen_radixtrie_node const *const pnode,
    int *const pstatus)
{
  return mxgen_radixtrie_put(ppool, skey, NULL, pnode, pstatus);
}

#endif
