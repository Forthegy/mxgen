#ifndef _MXGEN_LEVEL0_AIO_INCLUDE_
#define _MXGEN_LEVEL0_AIO_INCLUDE_

#include "mxgen_L0_base.h"
#include "mxgen_L0_htab.h"

/* On Windows we do not use UNIX file descriptors because the Windows
 * implementation of the POSIX low level I/O does not provide for non-blocking
 * file descriptors. Therefore, on Windows, we use address file handles and
 * internally wrap both UNIX file descriptor I/O and overlapped Windows I/O
 * so that their quite different behaviors will give rise to a uniform
 * programming interface.
 */
#if defined(MXGEN_LEVEL0_PLATFORM_FAMILY_MICROSOFT)
typedef void *mxgen_filedesc_t;
#else
typedef int mxgen_filedesc_t;
#endif

enum mxgen_aio_status
{
  mxgen_aio_status_read_complete,
  mxgen_aio_status_read_partial,
  mxgen_aio_status_read_eof,
  mxgen_aio_status_write_complete,
  mxgen_aio_status_write_partial,
  mxgen_aio_status_bad_filedesc,
  mxgen_aio_status_bad_operation,
  mxgen_aio_status_pipe_broken,
  mxgen_aio_status_access_denied,
  mxgen_aio_status_system_error
};

/* Procedure: mxgen_aio_read
 *
 * Read from an mxgen file descriptor to the extent possible if data are
 * available.
 *
 * Params:
 *   char *           pbuf  Destination buffer for data to be written to.
 *   long *           nbuf  Pointer to the length of the destination buffer for
 *                          data to be written to. The value at this address
 *                          will be overwritten by the actual number of bytes
 *                          read.
 *   mxgen_filedesc_t fd    File descriptor to read from.
 * Return: int
 *   == 0  read okay
 *    < 0  read failed
 *    > 0  end of file
 */
int
  mxgen_aio_read(
    char *const pbuf,
    long *const nbuf,
    mxgen_filedesc_t fd);

/* Procedure: mxgen_aio_write
 *
 * Write to an mxgen file descriptor to the extent possible.
 *
 * Params:
 *   char *           pbuf  Source buffer for data to be read from.
 *   long *           nbuf  Pointer to the length of the source buffer for data
 *                          to be read from. The value at this address will be
 *                          overwritten by the actual number of bytes written.
 *   mxgen_filedesc_t fd    File descriptor to write to.
 * Return: int
 *   == 0  read okay
 *    < 0  read failed
 *    > 0  end of file
 */
int
  mxgen_aio_write(
    char const *const pbuf,
    long *const nbuf,
    mxgen_filedesc_t fd);

/* Procedure: mxgen_aio_check
 *
 * Check what the status is of a given mxgen file descriptor.
 *
 * Params:
 *   int              status  Return value of an operation on the file
 *                            descriptor which should be checked.
 *   mxgen_filedesc_t fd      File descriptor to check.
 * Return: enum mxgen_aio_status
 */
MXGEN_LEVEL0_PURE
enum mxgen_aio_status
  mxgen_aio_check(
    int result,
    mxgen_filedesc_t fd);

#endif