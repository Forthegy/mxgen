#include "mxgen_L0_plat.h"
#include "mxgen_L0_cc.h"
#include "mxgen_L0_rdxtrie.h"

#include "../mxtest/mxtest.h"

MXTEST_BEGIN_SUITE(MyTests)
  MXTEST_BEGIN_TEST(Test1)
  MXTEST_END_TEST
  MXTEST_BEGIN_TEST(Test2)
    MXTEST_SET_FAILED;
  MXTEST_END_TEST
MXTEST_END_SUITE

int main(int argc, char *argv[])
{
  int exit_status;

  MXTEST_BEGIN(exit_status)
    MXTEST_EXEC(MyTests)
  MXTEST_END

  return exit_status;
}
