#ifndef _MXGEN_LEVEL0_BASE_INCLUDE_
#define _MXGEN_LEVEL0_BASE_INCLUDE_

#include "mxgen_L0_plat.h"
#include "mxgen_L0_line.h"
#include "mxgen_L0_cc.h"

#include <stdlib.h>
#include <fenv.h>
#include <limits.h>

#endif
