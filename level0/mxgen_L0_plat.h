#ifndef _MXGEN_LEVEL0_PLATFORM_INCLUDE_
#define _MXGEN_LEVEL0_PLATFORM_INCLUDE_

/* Microsoft family of platforms. */
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
# define MXGEN_LEVEL0_PLATFORM_KNOWN
# if defined(_WIN64)
#   define MXGEN_LEVEL0_PLATFORM_WINDOWS_64BIT
# else
#   define MXGEN_LEVEL0_PLATFORM_WINDOWS_32BIT
# endif
# define MXGEN_LEVEL0_PLATFORM_FAMILY_MICROSOFT
# if defined(_MSC_VER)
#   define MXGEN_LEVEL0_CUGLY
# else
#   define MXGEN_LEVEL0_CSTD
# endif
/* Apple family of platforms. */
#elif __APPLE__
# include <TargetConditionals.h>
# if TARGET_IPHONE_SIMULATOR
#   define MXGEN_LEVEL0_PLATFORM_SIMULATED_IOS
#   define MXGEN_LEVEL0_PLATFORM_KNOWN
# elif TARGET_OS_IPHONE
#   define MXGEN_LEVEL0_PLATFORM_IOS
#   define MXGEN_LEVEL0_PLATFORM_KNOWN
# elif TARGET_OS_MAC
#   define MXGEN_LEVEL0_PLATFORM_DARWIN
#   define MXGEN_LEVEL0_PLATFORM_KNOWN
# endif
# define MXGEN_LEVEL0_PLATFORM_FAMILY_APPLE
# define MXGEN_LEVEL0_CSTD
# define MXGEN_LEVEL0_CAPPLE
/* Probably UNIX. FIXME This should account for common lisp and other oddballs
 * like Temple. */
#else
# if defined(_POSIX_VERSION)
#   define MXGEN_LEVEL0_PLATFORM_POSIX
#   define MXGEN_LEVEL0_PLATFORM_KNOWN
#   define MXGEN_LEVEL0_PLATFORM_FAMILY_UNIX
#   define MXGEN_LEVEL0_CPOSIX
# endif
# if __linux__
#   define MXGEN_LEVEL0_PLATFORM_LINUX
#   define MXGEN_LEVEL0_PLATFORM_KNOWN
#   define MXGEN_LEVEL0_PLATFORM_FAMILY_UNIX
# elif __unix__
#   define MXGEN_LEVEL0_PLATFORM_UNIX
#   define MXGEN_LEVEL0_PLATFORM_KNOWN
#   define MXGEN_LEVEL0_PLATFORM_FAMILY_UNIX
# endif
# if defined(MXGEN_LEVEL0_PLATFORM_FAMILY_UNIX)
#   define MXGEN_LEVEL0_CSTD
# endif
#endif

/* WindowsNT */
#if defined(MXGEN_LEVEL0_PLATFORM_WINDOWS_64BIT) || defined(MXGEN_LEVEL0_PLATFORM_WINDOWS_32BIT)
# define MXGEN_LEVEL0_PLATFORM_WINDOWS
#endif

/* Report family. */
#if defined(MXGEN_LEVEL0_PLATFORM_FAMILY_UNIX)
# pragma message ( "mxgen level 0 detected UNIX platform family." )
#endif
#if defined(MXGEN_LEVEL0_PLATFORM_FAMILY_MICROSOFT)
# pragma message ( "mxgen level 0 detected Microsoft platform family." )
#endif
#if defined(MXGEN_LEVEL0_PLATFORM_FAMILY_APPLE)
# pragma message ( "mxgen level 0 detected Apple platform family." )
#endif

/* Report first pass knowledge of C environment. */
#if defined(MXGEN_LEVEL0_CSTD)
# pragma message ( "mxgen level 0 assuming standard C89 library is available." )
#else
# pragma message ( "mxgen level 0 assuming standard C89 library is not available." )
#endif
#if defined(MXGEN_LEVEL0_CUGLY)
# pragma message ( "mxgen level 0 assuming ugly C (msvcrt) library is available." )
#else
# pragma message ( "mxgen level 0 assuming ugly C (msvcrt) library is not available." )
#endif
#if defined(MXGEN_LEVEL0_CAPPLE)
# pragma message ( "mxgen level 0 assuming Apple C library extensions are available." )
#else
# pragma message ( "mxgen level 0 assuming Apple C library extensions are not available." )
#endif

/* Die if we don't know how to handle this platform. */
#if !defined(MXGEN_LEVEL0_PLATFORM_KNOWN)
# error "mxgen level 0 does not recognize the build platform"
#endif

/* Platform specific memory and string operations. */
#if defined(MXGEN_LEVEL0_PLATFORM_FAMILY_UNIX)     \
    || defined(MXGEN_LEVEL0_PLATFORM_FAMILY_APPLE) \
    || defined(MXGEN_LEVEL0_PLATFORM_POSIX)
# include <alloca.h>
# include <malloc.h>
# include <string.h>
/* memset is specially optimized by most compilers, just use that. */
# define mxgen_memclear(p, n) ((void)memset((void *)(p), 0, (size_t)(n)))
/* memcpy is specially optimized by most compilers, just use that. */
# define mxgen_memcpy(p, q, n) ((void)memcpy((void *)(p), (void const *)(q), (size_t)(n)))
/* Using standard strnlen. */
# define mxgen_strnlen(s, n) ((unsigned long)strnlen((char const *)(s), (size_t)(n)))
/* Stack frame allocation. */
# define mxgen_alloca(n) ((void *)alloca((size_t)(n)))
#elif defined(MXGEN_LEVEL0_PLATFORM_FAMILY_MICROSOFT)
# define VC_EXTRALEAN
# define WIN32_LEAN_AND_MEAN
# include <windows.h>
# include <malloc.h>
# include <string.h>
/* ZeroMemory is an artifact of the days in which memset was too slow on
 * Windows because NT only ran in virtual mode. */
# define mxgen_memclear(p, n) ((void)ZeroMemory((void *)(p), (size_t)(n)))
/* use builtin memcpy */
# define mxgen_memcpy(p, q, n) ((void)memcpy((void *)(p), (void const *)(q), (size_t)(n)))
/* strnlen on Windows has been declared both bad and poopy. */
# define mxgen_strnlen(s, n) ((unsigned long)strnlen_s((char const *)(s), (size_t)(n)))
/* Stack frame allocation. */
# define mxgen_alloca(n) ((void *)_alloca((size_t)(n)))
#else
# error "mxgen level 0 does not have a normalization for platform memory and string operations on the build platform"
#endif


#endif
