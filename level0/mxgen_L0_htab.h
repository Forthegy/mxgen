#ifndef _MXGEN_LEVEL0_HTAB_INCLUDE_
#define _MXGEN_LEVEL0_HTAB_INCLUDE_

#include "mxgen_L0_base.h"

/* This file implements a simple chained hash table. */

/* hash table key type enumeration */
enum mxgen_htab_key_type
{
  /* treat the key pointer as the address of a null terminated string */
  mxgen_htab_key_string,
  /* treat the key pointer as the value of the key itself (unsigned address) */
  mxgen_htab_key_address,
  /* treat the key pointer as the value of the key itself (int) */
  mxgen_htab_key_int,
  /* treat the key pointer as the value of the key itself (long) */
  mxgen_htab_key_long
};

/* hash table value type enumeration */
enum mxgen_htab_value_type
{
  /* treat the value pointer as the address of a null terminated string */
  mxgen_htab_value_string,
  /* treat the value pointer as the value of the key itself (unsigned address) */
  mxgen_htab_value_address,
  /* treat the value pointer as the address of a structure */
  mxgen_htab_value_struct
};

/* policy specification of a hash table */
struct mxgen_htab_policy
{
  enum mxgen_htab_key_type ekeytype;     /* hash table key type */
  enum mxgen_htab_value_type evaluetype; /* hash table value type */
  /* the size of the hash table value structure, assuming that
   * evaluetype = mxgen_htab_value_struct */
  long value_bytes;  
  long grow_ratio_n; /* numerator of the grow ratio   */
  long grow_ratio_d; /* denominator of the grow ratio */
};

struct mxgen_htab_entry
{

};

struct mxgen_htab
{

};

#endif
