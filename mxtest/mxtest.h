#ifndef _MXTEST_INCLUDE_
#define _MXTEST_INCLUDE_

#include <stdlib.h>
#include <stdio.h>

#define MXTEST_BEGIN_SUITE(SuiteName) \
int _mxtest_suite##SuiteName(int *ppassed, int *pcounted) \
{ \
  char const *_suite_name = #SuiteName; \
  int _mxtest_ok, _mxtest_counted; \
  int _mxtest_suite_unit_count; \
  int _mxtest_suite_unit_complete; \
  int _mxtest_suite_unit_passed; \
  _mxtest_counted = 0; \
  _mxtest_suite_unit_complete = 0; \
  _mxtest_suite_unit_count = 0; \
BEGIN_MXTESTS: \
  _mxtest_suite_unit_passed = 0; \
  if (_mxtest_counted) \
    printf("%s", "Beginning tests for " #SuiteName ".\n");

#define MXTEST_END_SUITE \
END_MXTESTS: \
  if (!_mxtest_counted && _mxtest_suite_unit_count > 0) \
  { \
    _mxtest_counted = 1; \
    goto BEGIN_MXTESTS; \
  } \
  if (_mxtest_suite_unit_count > 0) \
  { \
    printf("%s%s%s%d%s%d%s%03.2f%s", "Completed tests for ", \
      _suite_name, ", [", _mxtest_suite_unit_passed, "/", \
      _mxtest_suite_unit_count, "] (", \
      (float)( \
        100.0*( \
          (double)_mxtest_suite_unit_passed / \
          (double)_mxtest_suite_unit_count)), \
      "%).\n"); \
  } \
  else \
    printf("%s%s%s", "There are no units in the suite ", _suite_name, "!\n"); \
    pcounted[0] = _mxtest_suite_unit_count; \
    ppassed[0] = _mxtest_suite_unit_passed; \
  return _mxtest_suite_unit_passed; \
}

#define MXTEST_BEGIN_TEST(TestName) \
BEGIN_TEST_##TestName: \
{ \
  _mxtest_ok = 1; \
  if (_mxtest_counted) \
  { \
    printf("%s%s%s%d%s%d%s", "  Test ", _suite_name, "/" #TestName " (", \
    _mxtest_suite_unit_complete + 1, "/", _mxtest_suite_unit_count, ")..."); \
    fflush(stdout); \
  } \
  else \
    ++_mxtest_suite_unit_count;

#define MXTEST_END_TEST \
  if (_mxtest_counted) \
  { \
    if (_mxtest_ok) \
    { \
      printf("%s", "ok\n"); \
      ++_mxtest_suite_unit_passed; \
    } \
    else \
      printf("%s", "bad\n"); \
    ++_mxtest_suite_unit_complete; \
  } \
}

#define MXTEST_SET_FAILED _mxtest_ok = 0

#define MXTEST_BEGIN(ResultVar) \
{ \
  int *_mxtest_presult; \
  int _mxtest_passed, _mxtest_counted; \
  _mxtest_presult = &(ResultVar); \
  _mxtest_passed = _mxtest_counted = 0;

#define MXTEST_END \
  printf("Total of %d passed out of %d (%03.2f%%).", \
    _mxtest_passed, _mxtest_counted, \
    (float)(100.0*((double)_mxtest_passed/(double)_mxtest_counted))); \
  _mxtest_presult[0] = (_mxtest_passed == _mxtest_counted) ? 0 : 1;\
}

#define MXTEST_EXEC(SuiteName) \
  (void)_mxtest_suite##SuiteName(&_mxtest_passed, &_mxtest_counted);

#endif
